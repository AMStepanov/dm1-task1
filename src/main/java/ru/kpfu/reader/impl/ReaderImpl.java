package ru.kpfu.reader.impl;

import org.apache.commons.io.IOUtils;
import ru.kpfu.model.ARRF;
import ru.kpfu.model.Attribute;
import ru.kpfu.model.Data;
import ru.kpfu.reader.Reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Aleksandr Stepanov
 */
public class ReaderImpl implements Reader {

    public ARRF readARRFFile(File file) {
        try (FileInputStream fileInputStream = new FileInputStream(file)) {

            List tmp = IOUtils.readLines(fileInputStream);
            List<String> strings = (List<String>) tmp.stream().map(Object::toString).collect(Collectors.toList());
            String[] data = new String[strings.size()];
            strings.toArray(data);
            int i = 0;
            ARRF arrf = new ARRF();

            while (!data[i].startsWith("@relation")) i++;
            setNameRelation(data[i], arrf);

            while (!data[i].startsWith("@attribute")) i++;

            while (data[i].startsWith("@attribute")) {
                addAttribute(data[i], arrf);
                i++;
            }

            while (!data[i].startsWith("@data")) i++;
            i++;

            while (i < data.length) {
                addData(data[i], arrf);
                i++;
            }

            return arrf;
        } catch (IOException e) {
            return null;
        }
    }

    public ARRF readARRFFile(String path) {
        return readARRFFile(new File(path));
    }

    private void setNameRelation(String str, ARRF arrf) {
        arrf.setName(str.split(" ")[1]);
    }

    private void addAttribute(String str, ARRF arrf) {
        String[] tmp = str.split(" ");
        arrf.getAttributes().add(new Attribute(tmp[1], tmp[2])); //@TODO кастыль
    }

    private void addData(String str, ARRF arrf) {
        String[] data = str.split(",");
        arrf.getData().add(new Data(data));
    }
}
