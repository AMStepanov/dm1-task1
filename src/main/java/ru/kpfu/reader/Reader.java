package ru.kpfu.reader;

import ru.kpfu.model.ARRF;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author Aleksandr Stepanov
 */
public interface Reader {
    ARRF readARRFFile(File file);

    ARRF readARRFFile(String path);
}
