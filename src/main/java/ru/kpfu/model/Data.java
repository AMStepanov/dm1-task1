package ru.kpfu.model;

/**
 * @author Aleksandr Stepanov
 */
public class Data {

    private String[] data;

    public Data(String[] data) {
        this.data = data;
    }

    public String getData(int column) {
        return data[column];
    }

    public String[] getAllData() {
        return data;
    }
}
