package ru.kpfu.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Aleksandr Stepanov
 */
public class ARRF {

    private String name;
    private List<Attribute> attributes;
    private List<Data> data;

    public ARRF() {
        attributes = new ArrayList<Attribute>();
        data = new ArrayList<Data>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }
}
