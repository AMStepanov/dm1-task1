import ru.kpfu.model.ARRF;
import ru.kpfu.reader.Reader;
import ru.kpfu.reader.impl.ReaderImpl;

import java.io.IOException;

/**
 * @author Aleksandr Stepanov
 */
public class Test {
    public static void main(String[] args) throws IOException {
        Reader reader = new ReaderImpl();
        ARRF arrf = reader.readARRFFile("src/test/resources/airline.arff");
        System.out.println(arrf.getName());
        arrf.getAttributes().forEach(s -> System.out.println(s.getName()));
    }
}
